#ifndef TEST_CASES
#define TEST_CASES

#include <iostream>
#include <string>
#include <vector>
#include "ZOO.hpp"
#include "Enclosure.hpp"
#include "Keeper.hpp"
#include "Animal.hpp"

void AddingTest()
{
	//Test 1. Add a keeper and check if it exists
	//Add a keeper and an enclosure and check if they exist
	//Add an animal to an enclosure and check if it exists

	std::cout << "Now testing: creating keeper, enclosure, animal\n";

	Zoo Z;
	Z.create_keeper(1, "aaa");
	Z.create_enclosure(2, 1);
	Z.create_animal_in_enc(0, 1, "cat", "mruczek");

    if (Z.count_animals()==1 && Z.count_enclosures()==1 && Z.count_keepers()==1)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void AddingEnclosureBeforeKeeperTest()
{
	//Test 2. Try to add an enclosure without a keeper

	std::cout << "Now testing: creating enclosure without a keeper\n";

	Zoo Z;

    if (!Z.create_enclosure(1, 1))
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void AddingAnimalBeforeEnclosureTest()
{
	//Test 3. Try to add an animal without enclosure

	std::cout << "Now testing: creating animal without enclosure\n";

	Zoo Z;

    if (!Z.create_animal(1, "cat", "mruczek") && !Z.create_animal_in_enc(0, 1, "cat", "mruczek"))
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void AddToFullEnclosureTest()
{
	//Test 4. Try to add animal to full enclosure

    std::cout<<"Now testing: not allowing more animals than capacity\n";

    Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(1, 1);
    Z.create_animal(1, "dog", "burek");
    Z.create_animal(2, "dog", "reksio");

    if (Z.count_animals()==1)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void CopyConstructorTest()
{
    //Test 5. Copy construct a Zoo, remove the original and check if new one
    //still exists normally

    std::cout<<"Now testing: copy constructor\n";

    Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(1, 1);
    Z.create_animal(1, "dog", "burek");

    Zoo Z1(Z);
    Z.clear_all();

    if (Z1.count_animals()==1 && Z1.count_enclosures()==1 && Z1.count_keepers()==1 && Z1.keeper_with_id(1) != 0)  //add more checks in this line???
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void RemoveAnimalTest()
{
    //Test 6. Remove animal from enclosure and check if number of animals decreased

    std::cout<<"Now testing: removing animal\n";
    int before, after;

    Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(2, 1);
    Z.create_animal(1, "dog", "burek");
    Z.create_animal(2, "dog", "reksio");
    before = Z.count_animals();

    Z.remove_animal(2);
    after = Z.count_animals();

    if (after = before-1)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void RemoveAnimalFromEmptyTest()
{
    //Test 7. Try to remove animal from empty enclosure

    std::cout<<"Now testing: removing animal from empty enclosure\n";

    Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(2, 1);

    if (!Z.remove_animal(1))
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void RelocateAnimalTest()
{
    //Test 8. Relocate one of two animals from an enclosure

    std::cout<<"Now testing: relocating animal\n";

    Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(2, 1);
    Z.create_enclosure(2, 1);
    Z.create_animal(1, "dog", "burek");
    Z.create_animal(2, "dog", "reksio");

    Z.relocate_animal(1, 1);

    if (Z.count_animals_in_enc(0) == 1 && Z.count_animals_in_enc(1) == 1)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void RelocateAnimalToSameEncTest()
{
    //Test 9. Try to relocate an animal that is already in that enclosure

    std::cout<<"Now testing: relocating animal that is already in destination enclosure\n";

    Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(2, 1);
    Z.create_animal(1, "dog", "burek");

    if (!Z.relocate_animal(1, 0))
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void AddToNextEnclosureTest()
{
	//Test 10. In a Zoo: first enclosure full, check if animal was added to second enclosure

	std::cout<<"Now testing: adding animal to next enclosure\n";

    Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(1, 1);
    Z.create_enclosure(1, 1);
    Z.create_animal(1, "dog", "burek");
    Z.create_animal(2, "dog", "reksio");

    if (Z.count_animals_in_enc(0) == 1 && Z.count_animals_in_enc(1) == 1)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void RemoveEnclosureTest()
{
	//Test 11. Remove an enclosure and check: one animal moved to another enclosure,
	//		  one removed

	std::cout<<"Now testing: removing enclosure and its consequences\n";

	Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(2, 1);
    Z.create_enclosure(1, 1);
    Z.create_animal(1, "dog", "burek");
    Z.create_animal(2, "dog", "reksio");

    Z.remove_enclosure(0);

    if (Z.count_animals() == 1)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void ChangeKeeperTest()
{
	//Test 12. Change a keeper and check if number of assigned enclosures for both
	//        keepers changed correctly

	std::cout<<"Now testing: changing the assigned keeper\n";

	Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_keeper(2, "better keeper");
    Z.create_enclosure(2, 1);

    Z.change_keeper(0, 2);

    if (Z.keeper_min() == 1)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void ChangeKeeperAlreadyAssignedTest()
{
	//Test 13. Try to change keeper to a keeper that is already assigned to this enclosure

	std::cout<<"Now testing: changing the keeper that is already assigned to this enclosure\n";

	Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(2, 1);

    if (!Z.change_keeper(0, 1))
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void RemoveKeeperTest()
{
	//Test 14. Remove a keeper and check if other keeper has more assigned enclosures

	std::cout<<"Now testing: removing a keeper\n";

    Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_keeper(2, "better keeper");
    Z.create_enclosure(2, 1);

    Z.remove_keeper(1);

    if (Z.keeper_with_id(2)->get_num_enc() == 1)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void RemoveLastKeeperTest()
{
	//Test 15. Try to remove the only keeper in a list, which also has enclosures assigned

	std::cout<<"Now testing: removing last keeper\n";

	Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(2, 1);

    if (!Z.remove_keeper(1))
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void RemoveSpeciesTest()
{
	//Test 16. Remove species and check if number of animals decreased

	std::cout<<"Now testing: removing species\n";

	Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(3, 1);
    Z.create_animal(1, "dog", "burek");
    Z.create_animal(2, "dog", "reksio");
    Z.create_animal(3, "cat", "mruczek");

    Z.remove_species("dog");

    if (Z.count_animals() == 1)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void RemoveNonexistentSpeciesTest()
{
	//Test 17. Try to remove animals from species that is not in the zoo

	std::cout<<"Now testing: removing species that does not exist in the zoo\n";

	Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(2, 1);
    Z.create_animal(1, "dog", "burek");
    Z.create_animal(3, "cat", "mruczek");

    if (!Z.remove_species("giraffe") && Z.count_animals() == 2)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void BadParameterAddingTest()
{
    //Test 18. Various types of bad parameters for methods that create

    std::cout<<"Now testing: various bad parameters when adding\n";

    Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(1, 1);
    Z.create_animal(1, "dog", "burek");

    Z.create_keeper(-1, "aaa");  //bad keeper id
    Z.create_enclosure(1, 3);  //keeper doesn't exist
    Z.create_enclosure(-1, 1);  //bad capacity
    Z.create_animal_in_enc(6, 2, "dog", "burek");  //enclosure doesn't exist
    Z.create_animal(-1, "dog", "bad id");  //bad animal id
    Z.create_animal(1, "dog", "bad id");   //animal with this id already exists
    Z.create_keeper(1, "bad keeper");   //keeper with this id already exists

    if (Z.count_animals()==1 && Z.count_enclosures()==1 && Z.count_keepers()==1)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void BadParameterChangingTest()
{
    //Test 19. Various types of bad parameters for methods that change

    std::cout<<"Now testing: various bad parameters whan changing\n";

    Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(1, 1);
    Z.create_animal(1, "dog", "burek");

    if (!Z.change_keeper(6,1)   //bad enclosure index
        && !Z.change_keeper(0, 6)   //bad keeper id
        && !Z.relocate_animal(-6,0) )  //bad animal id
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void BadParameterRemovingTest()
{
    //Test 19. Various types of bad parameters for methods that change

    std::cout<<"Now testing: various bad parameters when removing\n";

    Zoo Z;
    Z.create_keeper(1, "good keeper");
    Z.create_enclosure(1, 1);
    Z.create_animal(1, "dog", "burek");

    Z.remove_keeper(-1);  //bad keeper id
    Z.remove_enclosure(7);  //bad index
    Z.remove_animal(-1);  //bad animal id
    Z.remove_species("cat");  //no animal of this species

    if (Z.count_animals()==1 && Z.count_enclosures()==1 && Z.count_keepers()==1)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

void RemoveFromEmptyZooTest()
{
    //Test 20. Try to remove and change stuff from empty Zoo

    std::cout<<"Now testing: removing things from empty Zoo\n";

	Zoo Z;
	bool t1 = Z.remove_animal(1);
	bool t2 = Z.remove_enclosure(1);
	bool t3 = Z.remove_keeper(1);
	bool t4 = Z.remove_species("dog");
	bool t5 = Z.change_keeper(1,1);
	bool t6 = Z.relocate_animal(1,1);

    if (!t1 && !t2 && !t3 && !t4 && !t5 && !t6)
        std::cout << "Success\n";
    else
        std::cout << "Fail\n";
}

#endif // TEST_CASES
