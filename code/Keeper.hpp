#ifndef KEEPER
#define KEEPER

#include <iostream>
#include <string>

class Keeper
{
	private:
	int keeper_id;  //unique and >1
	std::string keeper_name;
	int enclosures_assigned;  //number of enclosures to which the keeper is assigned.
                                //value is 0 when constructed
	public:
	Keeper(const int id, const std::string name);
	int get_id() const;
    std::string get_name() const;
    int get_num_enc() const;
    int& ref_num_enc();  //returns a reference to number of assigned enclosures
	friend std::ostream & operator<< (std::ostream& os, const Keeper &k);
};

Keeper::Keeper(const int id, const std::string name)
{
    if (id<=0)
    {
        std::cout<<"cannot create keeper. id parameter bad\n";
    }
    else
    {
        Keeper::keeper_id=id;
        Keeper::keeper_name=name;
        Keeper::enclosures_assigned=0;
    }
}

int Keeper::get_id() const
{
    return keeper_id;
}

std::string Keeper::get_name() const
{
    return keeper_name;
}

int Keeper::get_num_enc() const
{
    return enclosures_assigned;
}

int& Keeper::ref_num_enc()
{
    return this->enclosures_assigned;
}

std::ostream & operator<< (std::ostream& os, const Keeper& k)
{
    os<<"KEEPER id: "<<k.keeper_id<<"  name: "<<k.keeper_name<<"  num. of enc.: "<<k.enclosures_assigned<<"\n";
    return os;
}

#endif
