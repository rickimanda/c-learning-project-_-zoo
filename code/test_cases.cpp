#include <iostream>
#include "test_cases.hpp"

int main()
{
    AddingTest();
    AddingEnclosureBeforeKeeperTest();
    AddingAnimalBeforeEnclosureTest();
    AddToFullEnclosureTest();
    CopyConstructorTest();
    RemoveAnimalTest();
    RemoveAnimalFromEmptyTest();
    RelocateAnimalTest();
    RelocateAnimalToSameEncTest();
    AddToNextEnclosureTest();
    RemoveEnclosureTest();
    ChangeKeeperTest();
    ChangeKeeperAlreadyAssignedTest();
    RemoveKeeperTest();
    RemoveLastKeeperTest();
    RemoveSpeciesTest();
    RemoveNonexistentSpeciesTest();
    BadParameterAddingTest();
    BadParameterChangingTest();
    BadParameterRemovingTest();
    RemoveFromEmptyZooTest();
}
