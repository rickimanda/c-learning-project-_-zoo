#ifndef ANIMAL
#define ANIMAL

#include <iostream>
#include <string>

class Animal
{
	private:
	int animal_id;  //unique and >1
	std::string species;
	std::string animal_name;

	public:
	Animal(const int iid, const std::string sspecies, std::string nname);
	int get_id() const;
	std::string get_species() const;
	std::string get_name() const;
	friend std::ostream & operator<< (std::ostream& os, const Animal &a); //prints this
};

Animal::Animal(const int iid, const std::string sspecies, std::string nname)
{
    if (iid<=0)
    {
        std::cout<<"cannot create animal. id parameter bad\n";
    }
    else
    {
        Animal::animal_id=iid;
        Animal::species=sspecies;
        Animal::animal_name=nname;
    }
}

int Animal::get_id() const
{
    return animal_id;
}

std::string Animal::get_species() const
{
    return species;
}

std::string Animal::get_name() const
{
    return animal_name;
}

std::ostream & operator<< (std::ostream& os, const Animal& a)
{
    os<<"id: "<<a.animal_id<<"  species: "<<a.species<<"  name: "<<a.animal_name<<"\n";
    return os;
}

#endif
