#ifndef ENCLOSURE
#define ENCLOSURE

#include <iostream>
#include <string>
#include <vector>
#include "Keeper.hpp"
#include "Animal.hpp"

class Enclosure
{
	private:
	int capacity;  //>0
	std::vector <Animal> animals;
	Keeper* keeper_ptr;
	//enclosures are identified by their place in the vector

	public:
	Enclosure(int ccapacity, Keeper* ptr);
	~Enclosure();
	//creates animal with specified parameters in this object.
	//if the enclosure is full or species is not allowed, it doesn't
	bool add_animal(const int id, const std::string species, std::string name);
	int get_capacity() const;
	int get_occupation() const;  //returns how many animals in enclosure
	bool is_full() const;
	int id_of_first() const;  //returns id of first animal in this enclosure
	Keeper* get_keeper() const;
	Keeper*& ref_keeper();  //returns a reference to this keeper
	std::string species_with_id(const int id);   //returns species of animal with this id
    std::string name_with_id(const int id);   //returns name of animal with this id
    bool exists_animal(const int id);  //checks if this animal exists in this enclosure
    bool remove_animal (const int id);  //removes this animal from this enclosure
    bool remove_species (const std::string species);  //removes all animals from this species from enclosure
	friend std::ostream & operator<< (std::ostream& os, const Enclosure &e);
};

Enclosure::Enclosure(int ccapacity, Keeper* pptr)
{
    if (pptr==0)
    {
        std::cout<<"cannot create enclosure. null pointer\n";
    }
    else if(ccapacity<=0)
    {
        std::cout<<"cannot create enclosure. capacity parameter bad\n";
    }
    else
    {
        Enclosure::capacity=ccapacity;
        Enclosure::keeper_ptr=pptr;
        Enclosure::keeper_ptr->ref_num_enc()++;
    }

}

Enclosure::~Enclosure()
{
    animals.clear();
}

bool Enclosure::exists_animal(const int id)
{
    if (id>0)
    {
        for (int j=0; j<animals.size(); j++)
        {
            if (animals.at(j).get_id()==id)
                return 1;
        }
    }
    return 0;
}

std::string Enclosure::species_with_id(const int id)
{
    if (id<=0)
    {
        std::cout<<"species_with_id : parameters bad\n";
        return 0;
    }

    for (int j=0; j<animals.size(); j++)
    {
        if (animals.at(j).get_id()==id)
            return animals.at(j).get_species();
    }
    return 0;
}

std::string Enclosure::name_with_id(const int id)
{
    if (id<=0)
    {
        std::cout<<"name_with_id : parameters bad\n";
        return 0;
    }

    for (int j=0; j<animals.size(); j++)
    {
        if (animals.at(j).get_id()==id)
            return animals.at(j).get_name();
    }
    return 0;
}

bool Enclosure::remove_animal(const int id)
{
    if (id<=0)
    {
        std::cout<<"remove_animal : parameters bad\n";
        return 0;
    }

    for (int j=0; j<animals.size(); j++)
    {
        if (animals.at(j).get_id()==id)
        {
            animals.erase(animals.begin()+j);
            return 1;
        }
    }
    return 0;
}

bool Enclosure::remove_species(const std::string species)
{
    bool done=0;

    for (int j=animals.size()-1; j>=0; j--)
    {
        if (animals.at(j).get_species()==species)
        {
            animals.erase(animals.end()-j);
            done=1;
        }
    }
    return done;
}

int Enclosure::get_capacity() const
{
    return capacity;
}

int Enclosure::get_occupation() const
{
    return animals.size();
}

int Enclosure::id_of_first() const
{
    return animals.at(0).get_id();
}

bool Enclosure::is_full() const
{
    if (animals.size()<capacity)
        return 0;
    else if (animals.size()==capacity)
        return 1;
    else
    {
        std::cout<<"ERROR: ENCLOSURE IS MORE THAN FULL THIS SHOULDN'T HAPPEN!!!\n";
        return 0;
    }
}

Keeper* Enclosure::get_keeper() const
{
    return keeper_ptr;
}

Keeper*& Enclosure::ref_keeper()
{
    return this->keeper_ptr;
}

bool Enclosure::add_animal(const int id, const std::string species, std::string name)
{
    if (id<=0)
    {
        std::cout<<"parameters bad\n";
        return 0;
    }

    if (!is_full())
    {
        Animal A(id, species, name);
        animals.push_back(A);
        return 1;
    }
    std::cout<<"enclosure full, can't add animal\n";
    return 0;
}

std::ostream & operator<< (std::ostream& os, const Enclosure& e)
{
    if (e.animals.empty())
    {
        os<<"ENCLOSURE capacity: "<<e.capacity;
        os<<"\nenclosure empty\n";
        std::cout<<*e.keeper_ptr;
    }
    else
    {
        os<<"ENCLOSURE capacity: "<<e.capacity;
        os<<"\nanimals:\n";
        for(int i=0; i<e.animals.size(); i++)
        {
            os<<i<<". "<<e.animals.at(i);
        }
        std::cout<<*e.keeper_ptr;
    }
    return os;

}

#endif
