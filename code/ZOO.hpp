#ifndef ZOO
#define ZOO

#include <iostream>
#include <string>
#include <vector>
#include "Enclosure.hpp"
#include "Keeper.hpp"
#include "Animal.hpp"

class Zoo
{
	private:
	std::vector <Enclosure> enclosures;
	std::vector <Keeper*> keepers;
    bool exists_animal (const int id);  //checks if animal with that id exists
    int index_of_keeper(const int id) const;  //returns index of keeper with that id

	public:  //all methods have tests for correct parameters. if parameters are not correct, methods don't do anything
	Zoo();
	Zoo(const Zoo &z);
	void clear_all();  //basically the destructor
	~Zoo();
	//adds enclosure with these parameters to the end of the vector
	bool create_enclosure(const int capacity, int keeper_id);
	//removes enclosure that is at that index in the vector.
	//moves each animal to first free possible place in other enclosure.
	//if no other place possible, removes the animal
	bool remove_enclosure(int index);
	//adds a keeper with these parameters to the vector with keepers
	bool create_keeper(const int id, const std::string name);
	//removes the keeper with that id. all enclosures that had this keeper assigned,
	//get assigned a new keeper, the one that has the least enclosures assigned,
	//so that no enclosure is keeperless
	bool remove_keeper(const int id);
	//adds this animal to the first free postion possible
	//if all are full, it doesn't
	bool create_animal(const int id, const std::string species, std::string name);
	//adds this animal to the enclosure at this index. if it's full, it doesn't
	bool create_animal_in_enc(const int index, const int id, const std::string species, std::string name);
    bool change_keeper(const int index, const int keeper_id);  //changes the assigned keeper of the enclosure at that index
	bool remove_animal(const int id);  //removes the animal from the zoo
	bool relocate_animal(const int id, const int index);  //if possible, moves the animal to given enclosure
	bool remove_species(const std::string species);  //removes all animals of this species from the zoo
	int count_enclosures();
	int count_animals();  //count all animals
	int count_animals_in_enc(int index);
	int count_keepers();
	int keeper_min () const;  //returns id of keeper with least enclosures assigned.
	Keeper* keeper_with_id (const int id) const; //returns a pointer to a keeper with that id
	friend std::ostream & operator<< (std::ostream& os, const Zoo &z);
};

Zoo::Zoo()
{}

Zoo::Zoo(const Zoo &z)
{
    if (z.keepers.size() != 0 && z.enclosures.size() != 0)
    {
        for (int i=0; i<z.keepers.size(); i++)  //copy all keepers (make new pointers)
        {
            int temp_id = z.keepers.at(i)->get_id();
            std::string temp_name = z.keepers.at(i)->get_name();

            Keeper* K=new Keeper (temp_id, temp_name);
            Zoo::keepers.push_back(K);
        }

        enclosures = z.enclosures;  //copy all enclosures

        for (int i=0; i<Zoo::enclosures.size(); i++)  //fix pointers
        {
            Zoo::change_keeper(i, z.enclosures.at(i).get_keeper()->get_id());
        }
    }
}

void Zoo::clear_all()
{
    enclosures.clear();
    for(int i=0; i<keepers.size(); i++)
    {
        delete keepers.at(i);
    }
    keepers.clear();
}

Zoo::~Zoo()
{
    clear_all();
}

Keeper* Zoo::keeper_with_id (const int id) const
{
    if(id>0)
    {
        for (int i=0; i<this->keepers.size(); i++)
        {
            if (this->keepers.at(i)->get_id()==id)
                return keepers.at(i);
        }
    }
    return 0;
}

bool Zoo::exists_animal(const int id)
{
    if (enclosures.size() != 0)
    {
        for (int i=0; i<enclosures.size(); i++)
        {
            if (enclosures.at(i).exists_animal(id))
                return 1;
        }
        return 0;
    }
    else
        return 0;
}

int Zoo::keeper_min() const
{
    if(keepers.size() ==    0)
        return 0;

    int minim=9999999, minim_id=keepers.at(0)->get_id();

    for (int i=0; i<keepers.size(); i++)
    {
        if (keepers.at(i)->get_num_enc()<=minim)
        {
            minim=keepers.at(i)->get_num_enc();
            minim_id=keepers.at(i)->get_id();
        }
    }
    return minim_id;
}

int Zoo::index_of_keeper(const int id) const
{
    for (int i=0; i<keepers.size(); i++)
    {
        if (this->keepers.at(i)->get_id()==id)
            return i;
    }
}

bool Zoo::create_enclosure(const int capacity, int keeper_id)
{
    if(capacity<1||keeper_id<1||keeper_with_id(keeper_id)==0)
    {
        std::cout<<"create_enclosure : parameters bad\n";
        return 0;
    }
    else
    {
        Enclosure E(capacity, keeper_with_id(keeper_id));
        enclosures.push_back(E);
        return 1;
    }
}

bool Zoo::remove_enclosure(int index)
{
    if( enclosures.size() != 0 && index>=0 && index<enclosures.size())
    {
        bool done = 0;
        int occupation = enclosures.at(index).get_occupation();

        for(int i =0; i<occupation; i++)
        {

            int id = enclosures.at(index).id_of_first();
            done = 0;

            for(int j=0; j<enclosures.size() ; j++)
            {
                if (!enclosures.at(j).is_full() && j!=index && done==0)
                {
                    relocate_animal(id, j);
                    done=1;
                }
            }

            if (done==0)
            {
                remove_animal(id);
            }
        }

        if (enclosures.at(index).get_occupation()==0)
        {
            enclosures.at(index).get_keeper()->ref_num_enc()--;
            enclosures.erase(enclosures.begin()+index);
            return 1;
        }
        else
        {
            std::cout<<"remove_enclosure : this shouldn't happen\n";
            return 0;
        }
    }
    else
    {
        std::cout<<"remove_enclosure : parameters bad\n";
        return 0;
    }
}

bool Zoo::change_keeper(const int index, const int keeper_id)
{
    if(keepers.size()==0||index<0||index>enclosures.size()||keeper_id<1||keeper_with_id(keeper_id)==0)
    {
        std::cout<<"change keeper: parameters bad\n";
    }
    else if(enclosures.at(index).get_keeper() == keeper_with_id(keeper_id))
    {
        std::cout<<"change keeper: this keeper already assigned to this enclosure\n";
    }
    else
    {
        Keeper* temp = enclosures.at(index).get_keeper();
        enclosures.at(index).ref_keeper()=keeper_with_id(keeper_id);
        //adjust number of enclosures for each
        temp->ref_num_enc()--;
        enclosures.at(index).get_keeper()->ref_num_enc()++;
        return 1;
    }
    return 0;
}

bool Zoo::create_keeper(const int id, const std::string name)
{
    if(id<1 || keeper_with_id(id)!=0)
    {
        std::cout<<"create_keeper : parameters bad\n";
        return 0;
    }
    else
    {
        Keeper* K=new Keeper (id, name);
        keepers.push_back(K);
        return 1;
    }
}

bool Zoo::remove_keeper(const int id)
{
    if (id>0 && keeper_with_id(id)!=0)
    {
        if (keepers.size()==0)
        {
            std::cout<<"remove_keeper : no keepers\n";
            return 0;
        }
        else if (keepers.size()==1 && enclosures.size()>0)
        {
            std::cout<<"remove_keeper : cannot remove last keeper\n";
            return 0;
        }
        else
        {
            for (int i=0; i<enclosures.size(); i++)
            {
                if (enclosures.at(i).get_keeper()->get_id()==id)
                {
                    change_keeper(i, keeper_min());
                }
            }
            if (keeper_with_id(id)->get_num_enc()==0)
            {
                delete keepers.at(index_of_keeper(id));
                keepers.erase(keepers.begin()+index_of_keeper(id));
                return 1;
            }
            else
            {
                std::cout<<"remove_keeper : this shouldn't happen\n";
                return 0;
            }
        }
    }
    else
    {
        std::cout<<"remove_keeper : parameters bad\n";
        return 0;
    }
}

bool Zoo::create_animal_in_enc(const int index, const int id, const std::string species, std::string name)
{
    if (index<0||index>=enclosures.size()||id<1||exists_animal(id))
    {
        std::cout<<"create_animal_in_enc : parameters bad\n";
        return 0;
    }
    else
    {
        if(enclosures.at(index).get_occupation()<enclosures.at(index).get_capacity())
        {
            enclosures.at(index).add_animal(id, species, name);
            return 1;
        }
        else
        {
            std::cout<<"create_animal_in_enc : enclosure full\n";
            return 0;
        }
    }
}

bool Zoo::create_animal(const int id, const std::string species, std::string name)
{
    if (id<1||exists_animal(id)||enclosures.size()==0)
    {
        std::cout<<"create_animal : parameters bad\n";
        return 0;
    }
    else
    {
        for (int i=0; i<enclosures.size(); i++)
        {
            if(enclosures.at(i).get_occupation()<enclosures.at(i).get_capacity())
            {
                enclosures.at(i).add_animal(id, species, name);
                return 1;
            }
        }
        std::cout<<"create_animal : all enclosures full\n";
        return 0;
    }
}

bool Zoo::remove_animal(const int id)
{
    if (exists_animal(id))
    {
        for (int i=0; i<enclosures.size(); i++)
        {
            if (enclosures.at(i).exists_animal(id))
            {
                enclosures.at(i).remove_animal(id);
                return 1;
            }
        }
    }
    else
    {
        std::cout<<"remove_animal : parameters bad\n";
        return 0;
    }
}

bool Zoo::remove_species(const std::string species)
{
    bool done=0;

    if(enclosures.size() != 0)
    {
        for (int i=0; i<enclosures.size(); i++)
        {
            if(enclosures.at(i).remove_species(species))
                done=1;
        }
        if(done==0)
            std::cout<<"remove_species: parameters bad\n";
        return done;
    }
    else
        return 0;
}

bool Zoo::relocate_animal(const int id, const int index)
{
    if (id>0&&exists_animal(id)&&index>=0&&index<enclosures.size())
    {

        if(enclosures.at(index).exists_animal(id))
        {
            std::cout<<"relocate_animal : animal already in enclosure\n";
            return 0;
        }

        if (!enclosures.at(index).is_full())
        {
            int temp_ind;
            std::string temp_spe, temp_name;

            for(int i=0; i<enclosures.size(); i++)
            {
                if(enclosures.at(i).exists_animal(id))
                {
                    temp_ind=i;
                    temp_name=enclosures.at(i).name_with_id(id);
                    temp_spe=enclosures.at(i).species_with_id(id);
                    break;
                }
            }
            enclosures.at(index).add_animal(id, temp_spe, temp_name);
            enclosures.at(temp_ind).remove_animal(id);
            //i know that above works with the assumption that nothing's going to go wrong between copying and deleting
        }
        else
        {
            std::cout<<"relocate_animal : enclosure full\n";
            return 0;
        }
    }
    else
    {
        std::cout<<"relocate_animal : parameters bad\n";
        return 0;
    }
}

int Zoo::count_enclosures()
{
    return enclosures.size();
}

int Zoo::count_keepers()
{
    return keepers.size();
}

int Zoo::count_animals()
{
    int counter=0;
    for (int i=0; i<enclosures.size(); i++)
    {
        counter+=enclosures.at(i).get_occupation();
    }
    return counter;
}

int Zoo::count_animals_in_enc(int index)
{
    return enclosures.at(index).get_occupation();
}

std::ostream & operator<< (std::ostream& os, const Zoo &z)
{
    if (z.enclosures.empty()&&z.keepers.empty())
    {
        os<<"\nZOO empty\n";
    }
    else if (z.enclosures.empty())
    {
        os<<"\nZOO all enclosures empty\n"<<"keepers:\n";
        for(int i=0; i<z.keepers.size(); i++)
        {
            os<<i<<". "<<*z.keepers.at(i);
        }
        os<<"\n";
    }
    else if (z.keepers.empty())
    {
        std::cout<<"\n this shouldn't happen \n";
    }
    else
    {
        os<<"\nZOO enclosures:\n";
        for(int i=0; i<z.enclosures.size(); i++)
        {
            os<<i<<". "<<z.enclosures.at(i);
        }
        os<<"zookeepers:\n";
        for(int i=0; i<z.keepers.size(); i++)
        {
            os<<i<<". "<<*z.keepers.at(i);
        }
        os<<"\n";
    }
    return os;
}

#endif
